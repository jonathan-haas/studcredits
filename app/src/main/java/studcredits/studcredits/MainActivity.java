package studcredits.studcredits;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.PowerManager;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    // Time left in class
    private static final long TOTAL_TIME = 120000; // 2 min

    // Notification Channel ID
    private static final String CHANNEL_ID = "StudCredits";

    // UI Components
    private TextView timerTextView;
    private Button startButton;
    private ProgressBar timerProgress;
    private TextView creditCounter;

    // Variables to help keep track of if app is in foreground
    private PowerManager.WakeLock wakeLock;
    private PowerManager pm;


    // Internal book-keeping variables for earning points
    private long classTimeLeftMillis;
    private CountDownTimer countDownTimer;
    private int totalPoints = 0;
    private boolean earning;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        createNotificationChannel();
        setContentView(R.layout.activity_main);
        pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
        /*
         * Initialize variables
         */
        earning = false;
        creditCounter = findViewById(R.id.creditCounter);
        creditCounter.setText(Integer.toString(totalPoints));
        classTimeLeftMillis = TOTAL_TIME;
        timerTextView = findViewById(R.id.timerText);
        String resetTime = getFormattedTime(TOTAL_TIME);
        timerTextView.setText(resetTime);
        timerProgress = findViewById(R.id.timerProgress);
        timerProgress.setProgress(100);
        startButton = findViewById(R.id.startButton);
        startButton.setEnabled(true);
        startButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (!earning) {
                    wakeLock =
                            pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "StudCredits:CreditLock");
                    startTimer();
                    startButton.setEnabled(false);
                }
            }
        });
    }

    @Override
    protected void onPause() {
        super.onPause();
        if( earning && pm.isInteractive() ) {
            stopTimer();
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        if( earning && pm.isInteractive() ) {
            stopTimer();
        }
    }

    /**
     * Starts the count down timer
     */
    private void startTimer() {
        countDownTimer = new CountDownTimer(classTimeLeftMillis, 1000) {
            @Override
            public void onTick(long l) {
                // set time left
                classTimeLeftMillis = l;
                updateTimerText();
                updateProgress();
            }

            @Override
            public void onFinish() {
                // Reward points when the timer completes
                totalPoints += 1000;
                creditCounter.setText(Integer.toString(totalPoints));
                reset();
            }
        };
        if (!wakeLock.isHeld()) {
            wakeLock.acquire();
        }
        countDownTimer.start();
        earning = true;
    }

    /**
     * Updates the timer text with the current time left in class
     */
    private void updateTimerText() {
        String newTime = getFormattedTime(classTimeLeftMillis);
        timerTextView.setText(newTime);
    }

    /**
     * Format time string
     * @param timeInMillis time in milliseconds to format
     * @return {@link String} in mm:ss format
     */
    private String getFormattedTime(final long timeInMillis) {
        int min = (int) (timeInMillis / 60000);
        int sec = (int) (timeInMillis % 60000 / 1000);
        return String.format("%02d:%02d", min, sec);
    }

    /**
     * Updates the progress bar based on time left in class
     */
    private void updateProgress() {
        int progress = (int) ( (double)classTimeLeftMillis / TOTAL_TIME * 100);
        timerProgress.setProgress(progress);
    }


    /**
     * Cancel the timer without accruing credits and reset state
     */
    private void stopTimer() {
        warnUser();
        countDownTimer.cancel();
        reset();
    }

    /**
     * Create notification warning user that they have lost all credits for this session
     */
    private void warnUser() {
        NotificationCompat.Builder nBuilder =
                new NotificationCompat.Builder(this, CHANNEL_ID)
                        .setSmallIcon(R.drawable.ic_warning_black_24dp)
                        .setContentTitle("StudCredits")
                        .setStyle(new NotificationCompat.BigTextStyle()
                            .bigText("You've left the application " +
                                    "and will not earn credits for this session"))
                        .setPriority(NotificationCompat.PRIORITY_HIGH);
        Notification warning = nBuilder.build();
        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(this);
        notificationManager.notify(1, warning);
    }

    /**
     * Create notification channel for app
     */
    private void createNotificationChannel() {
        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = getString(R.string.channel_name);
            String description = getString(R.string.channel_description);
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel channel = new NotificationChannel(CHANNEL_ID, name, importance);
            channel.setDescription(description);
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this
            NotificationManager notificationManager = getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);
        }
    }

    /**
     * Reset after cancelling or finishing timer
     */
    private void reset() {
        if (wakeLock.isHeld()) {
            wakeLock.release();
        }
        String resetTime = getFormattedTime(TOTAL_TIME);
        timerTextView.setText(resetTime);
        classTimeLeftMillis = TOTAL_TIME;
        timerProgress.setProgress(100);
        startButton.setEnabled(true);
        earning = false;
    }
}
